package es.eltrueno.mc.bsw.game.antiglitch;

import java.util.HashMap;

import net.minecraft.server.v1_8_R3.EntityPlayer;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.util.Vector;

import es.eltrueno.mc.bsw.main;

public class AntiglitchManager {
	
	private static int antiglitch_task = -1;
	private static HashMap<Player, Location> PlayerLastLocation = new HashMap<Player, Location>();
	
	private static int getPing(Player p)
	{
		CraftPlayer cp = (CraftPlayer) p;
		EntityPlayer ep = cp.getHandle();
		return ep.ping;
	}
	
	public static void setLastLocation(Player p, Location loc){
		if(PlayerLastLocation.containsKey(p)){
			PlayerLastLocation.replace(p, loc);
		}
		else PlayerLastLocation.put(p, loc);
	}
	
	private static Location getLastLocation(Player p){
		if(PlayerLastLocation.containsKey(p)){
			return PlayerLastLocation.get(p);
		}
		else return null;
	}
	
	public static void Check(final Player p){
		if(antiglitch_task!=-1){
			final Location lastLoc = getLastLocation(p);
			setLastLocation(p, p.getLocation());
			if(ConfigManager.get("Config.yml").getBoolean("AntiGlitch.enable")){
				if(main.PLAYING.contains(p)&& !p.isOnGround() &&!p.isFlying() &&p.getGameMode()
						!=GameMode.CREATIVE && lastLoc.equals(p.getLocation())){
					if(!Bukkit.getScheduler().isQueued(antiglitch_task)){
						final Snowball snowball = p.getWorld().spawn(p.getLocation().add(0,1,0), Snowball.class);
						snowball.setVelocity(new Vector(0,-1,0));   
						antiglitch_task = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable(){
							public void run(){
								if(main.PLAYING.contains(p)&& !p.isOnGround() &&!p.isFlying() &&p.getGameMode()
										!=GameMode.CREATIVE && lastLoc.equals(p.getLocation())){
                      //Player is ussing the glitch!
								}
								snowball.remove();
								Bukkit.getScheduler().cancelTask(antiglitch_task);
							}
						}, (getPing(p)/4));
					}
				}
			}
		}
		else{
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable(){
				public void run(){
					final Location lastLoc = getLastLocation(p);
					setLastLocation(p, p.getLocation());
					if(ConfigManager.get("Config.yml").getBoolean("AntiGlitch.enable")){
						if(main.PLAYING.contains(p)&& !p.isOnGround() &&!p.isFlying() &&p.getGameMode()
								!=GameMode.CREATIVE && lastLoc.equals(p.getLocation())){
							if(!Bukkit.getScheduler().isQueued(antiglitch_task)){
								final Snowball snowball = p.getWorld().spawn(p.getLocation().add(0,1,0), Snowball.class);
								snowball.setVelocity(new Vector(0,-1,0));   
								antiglitch_task = Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(main.plugin, new Runnable(){
									public void run(){
										if(main.PLAYING.contains(p)&& !p.isOnGround() &&!p.isFlying() &&p.getGameMode()
												!=GameMode.CREATIVE && lastLoc.equals(p.getLocation())){
                          //Player is ussing the bug
										}
										snowball.remove();
										Bukkit.getScheduler().cancelTask(antiglitch_task);
									}
								}, (getPing(p)/4));
							}
						}
					}
				}
			}, 35);
		}
	}
}
